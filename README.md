# RIME 許氏注音方案

[改造自此](https://gist.githubusercontent.com/fillmember/e5192088e33b660ab2707013b86cf09c/raw/60c19609c086592915f70809aef04a5b51772ba7/bopomofo_hsuq.schema.yaml)
希望這程式有幫助，但不保證一定有。

# 改變

* 空白鍵輸入一聲
* 分字直虛線
* 增加上屏注音（SHIFT+ENTER）、原英文輸入（CTRL+ENTER）

# 依賴

* `terra-pinyin`
* `cangjie5`
